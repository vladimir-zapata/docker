DOCKER COMMANDS

***BUILD***
docker build <path>

***RUN***	
docker run <image>
docker run -it <image>
docker run -p <localhost-port>:<docker-port>